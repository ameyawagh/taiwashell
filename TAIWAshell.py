#-----------------------------------------------------------------------------------------------------------------------#
#File: TAIWAshell.py
#Author: Ameya Wagh
#Created on: 29th August 2016
#Co-Authors: Aditya Shinde
#Project:TAIWAv2
#Subproject: TAIWAshell
#Company: -
#Description: A reduced higher language shell script independent of OS in a verb noun fashion

#-----------------Includes----------------------------------------------------------------------------------------------#
import os
import sys
from subprocess import call
import json
import credentials as c
import vlc

global dflag
dflag = True

#--------------Class and function Declarations------ -------------------------------------------------------------------#
ignorelist = [".git"]
	

class Shell():
	def __init__(self):
		self.rootdir = ""		
		self.currdir = os.getcwd()
		self.ignore_dirs = os.path.join(self.currdir,"ignore_dir_list.txt")
		#self.U = Util()
		self.response = {'message':"",'error':0,'status':1}
		self.Keywords = {'Music':c.music_dir}
		if os.name == 'posix': self.os_name = 'Linux'
		elif os.name == 'nt': self.os_name = 'Windows'
	
	def Banner(self):
		self.debug("\n")
		self.debug("|","-"*50,"|")
		self.debug("\n\t\tTAIWAshell v1.0 \t\t\n")
		self.debug("|","-"*50,"|")
		self.debug("\n")

	def debug(self,debugstr,D2="",D3=""):
		if dflag == True:
			print debugstr,D2,D3
		else:
			print "debug false"	

	def errorDict(_error):
		error = {0:"null",
		1:"query error",
		2:"already exists",
		3:"could not delete",
		4:"directory not found",
		5:"could not read file",
		6:"file doesn't exist",
		7:"cannot execute file",
		8:"path not found"
		}
		return error[_error]

	def _help(self,param):
		help_str = '''
---------------------------------------------------------------------------------
			TAIWAshell usage													
---------------------------------------------------------------------------------
commands				Description
---------------------------------------------------------------------------------
list 				-To list contents of current directory
goto <directory>		-Navigate to the directory
create <file/dir>		-creates a file if extension provided with _name
				 Eg: test.txt
				 creates a directory if only name provided
				 Eg: test
delete <file/dir>		-Deletes any files or directories								
read <file> 			-Reades a file
write <file> <data> 		-Writes data to a file
run <file>			-executes if given is a python file	
help 				-shows help
exit 				-To exit shell
---------------------------------------------------------------------------------
'''
		self.response['response'] = None
		return help_str

	
	def list(self,param):
		#self.debug(os.listdir(self.currdir))
		if len(param)>1:				
			self.debug("-"*50)
			self.debug("\n")
			self.response['status'] = 1
			self.response['error'] = 0
			if param[1] in self.Keywords.keys():
				self.response['response'] = json.dumps(self.walk_dir(self.Keywords[param[1]]), indent=2, sort_keys = False)
			else:
				self.response['response'] = json.dumps(self.walk_dir(param[1]), indent=2, sort_keys = False)
		elif len(param) == 1:
			self.debug("-"*50)
			self.debug("\n")
			self.response['status'] = 1
			self.response['error'] = 0
			self.response['response'] = json.dumps(self.walk_dir(self.currdir), indent=2, sort_keys = False)	
		else:
			self.response['status'] = 0
			self.response['error'] = 8
			self.response['message'] = 1
			self.response['response'] = None
		
				
	def ignorelist(self,ignorelist_path):
	    fp = open(ignorelist_path,"r")
	    ignorelist = fp.readlines()
	    excludelist = []
	    for _dir in ignorelist:
	        excludelist.append(_dir.replace("/","").replace("\n",""))
	    fp.close()
	    return excludelist

	def walk_dir(self,path):
		d = {'name': os.path.basename(path)}
		excludelist = self.ignorelist(self.ignore_dirs)
		if os.path.isdir(path):
			d['type'] = "directory"
			d['children'] = [self.walk_dir(os.path.join(path,x)) for x in os.listdir(path) if x not in excludelist]
		else:
			d['type'] = "file"
		return d        	    		 

	def create(self,param):
		_name=param[1]		
		if "." in _name:
			if os.path.isfile(_name) is not True:
				f = open(_name,"w+")
				f.close()
				self.response['status'] = 1
				self.response['error'] = 0
				self.response['response'] = os.path.join(self.currdir,_name)
				return "File created"
			else:
				self.response['status'] = 0
				self.response['error'] = 2
				self.response['response'] = None
				return "File already exists" 		
		elif os.path.isdir(_name) is not True:
			os.mkdir(_name)
			self.response['status'] = 1
			self.response['error'] = 0
			self.response['response'] = os.path.join(self.currdir,_name)
			return "Directory created"
		else:
			self.response['status'] = 0
			self.response['error'] = 2
			self.response['response'] = None
			return "already exists"	
		
	def delete(self,param):
		_name = param[1]
		if os.path.isdir(_name):
			os.removedirs(_name)
			self.response['status'] = 1
			self.response['error'] = 0
			self.response['response'] = self.currdir
			return "removed directory"
		elif os.path.isfile(_name):
			os.remove(_name)
			self.response['status'] = 1
			self.response['error'] = 0
			self.response['response'] = self.currdir
			return "removed file"
		else:
			self.response['status'] = 0
			self.response['error'] = 3
			self.response['response'] = None
			return "error, could not delete"		

	def goto(self,param):
		paramlength = len(param)
		for i in range(1,paramlength):
			_name = param[i]
			self.debug(_name)			
			if os.path.isdir(_name):
				os.chdir(_name)
				self.currdir = os.getcwd()
				self.response['status'] = 1
				self.response['error'] = 0
				self.response['response'] = self.currdir
			elif _name == "..":
				os.chdir("..")
				self.currdir = os.getcwd()
				self.response['status'] = 1
				self.response['error'] = 0
				self.response['response'] = self.currdir	
			else: 
				self.response['status'] = 0
				self.response['error'] = 4
				self.response['response'] = None
				return "directory not found"
		return "In "+os.path.basename(self.currdir)		

	def read(self,param):
		_name = param[1]
		if os.path.isfile(_name):
			fp=open(_name,"r")
			cat=fp.readlines()
			fp.close()
			self.response['status'] = 1
			self.response['error'] = 0
			self.response['response'] = cat
			return "file read successfully"
		else:
			self.response['status'] = 0
			self.response['error'] = 5
			self.response['response'] = None
			return "could not read file"	

	def write(self,param):
		if os.path.isfile(param[1]):
			fp=open(param[1],"w")
			if param[2]:
				paramlength = len(param)	
				for i in range(2,paramlength):
					fp.write(param[i]+" ")
			fp.close()
			self.response['status'] = 1
			self.response['error'] = 0
			self.response['response'] = param[2:]
			return "done writing"
		else: 
			self.response['status'] = 0
			self.response['error'] = 6
			self.response['response'] = None
			return "file doesn't exist"

	def run(self,param):
		_name = param[1]
		if os.path.isfile(_name) and ".py" in _name:
			call(["python",_name])
			self.response['status'] = 1
			self.response['error'] = 0
			self.response['response'] = None
			return "successfully executed"
		else : 
			self.response['status'] = 0
			self.response['error'] = 7
			self.response['response'] = None
			return "cannot execute file"	

	def credentials(self,param):
		self.response['status'] = 1
		self.response['error'] = 0
		self.response['response'] = None
		return (c.email_id,c.password,self.os_name)

	def playMusic(self,param):
		global p
		p=vlc.MediaPlayer('file://'+self.Keywords[param[1]]+'/'+param[2])
		p.play()						

	def stopMusic(self,param):
		p.stop()

	def shell_handler(self,inputJson):
		shell_routine = {"list":self.list,
		"goto": self.goto,
		"create": self.create,
		"delete": self.delete,
		"read": self.read,
		"write": self.write,
		"run": self.run,
		"cred":self.credentials,
		"play":self.playMusic,
		"stop":self.stopMusic,
		"exit": quit,
		"help": self._help}

		param = json.loads(inputJson)['query'] 

		self.response['query'] = param
		if param[0] in shell_routine.keys():
			self.response['message']= shell_routine[param[0]](param)
			return json.dumps(self.response)
		else : 
			self.response['status'] = 0
			self.response['response'] = "null"
			self._help(param)
			self.response['error'] = 1
			return json.dumps(self.response)
		
#-----------------Main-------------------------------------------------------------------------------------------------#
def main():
	if "-v" in sys.argv: 
		global dflag
		dflag = True
						
		
	#globaldebug("Hello world")
	shell = Shell()
	shell.Banner()
	shell.debug("Verbose ON")
	#self.debug shell.list()
	#shell.create("Ameya")
	#shell.delete("Ameya")
	while True:
		try:
			cmd = raw_input(">>")
			print(cmd.split(" "))
			shell.shell_handler(cmd.split(" "))
			
		except KeyboardInterrupt:
			print("\n quit")
			quit()	 
#------------------------------------------------------
if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		quit()		
