## TAIWAshell
TAIWAshell is a python based windows and linux compatible **verb-noun** shell interface created for the project
** T.A.I.W.A (The Artificial Intelligence Workplace Assistant) version 2 **.


## How to use

'''

		#TAIWAshell usage													


commands				Description

list 					-To list contents of current directory
goto <directory>		-Navigate to the directory
create <file/dir>		-creates a file if extension provided with _name
				 		 Eg: test.txt
				 		 creates a directory if only name provided
				 		 Eg: test
delete <file/dir>		-Deletes any files or directories								
read <file> 			-Reades a file
write <file> <data> 	-Writes data to a file
run <file>				-executes if given is a python file
help 					-shows help
exit 					-To exit shell

'''

## Code Example

To list all directories and files as a tree
'''
>>list  
taiwashell  
    TAIWAshell.py  
    contributors.txt  
    test.py  
    ignore_dir_list.txt  
    README.md  
    .gitignore  


    Ameya  
        test1.py  
        test.txt  


        testdir  
            test2.py  

'''

To navigate to a directory
'''
>>goto Ameya testdir
Param--> ['goto', 'Ameya', 'testdir']
Ameya  
testdir  
'''

To read file content
'''
>>read test2.py
['print "My name is Ameya" ']
'''

To run a python file
'''
>>run test2.py
My name is Ameya
'''

To create directories or files using create command
'''
>>create test
Directory created

>>create test.py
File created
'''

To write to a file
'''
>>write test.py for i in range(1,5):print(i,"hello world")
done writing

>>run test.py
(1, 'hello world')
(2, 'hello world')
(3, 'hello world')
(4, 'hello world')

'''
To delete a file or a directory use delete command
'''
>>list
testdir  
    test.py  
    test2.py  


    test  

>>delete test
>>list
testdir  
    test.py  
    test2.py
'''

To exit shell
'''
>>exit
ameya@UltimateLinuxMachine:~/MLProjects/TAIWAshell/taiwashell$
'''

## Dependencies

You require the following python2.7 modules
* OS
* Subprocess
* Sys

#

## Contributors

Ameya Wagh <ameya555@gmail.com>
Aditya Shinde <adie.marine@gmail.com>

## License
